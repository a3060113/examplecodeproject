package onlyoriginal.restapi.repository;

import onlyoriginal.restapi.model.dto.tables.TableParam;
import onlyoriginal.restapi.model.entity.Item;
import onlyoriginal.restapi.utils.TestEntitiesGenerator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;


@Ignore
public class GenericRepositoryTest extends Assert {


    /**
     * TestEntityManager
     * <p>
     * Work with in memory H2 Database
     */

    @Autowired
    protected TestEntityManager em;

    Item item1;
    Item item2;
    Item item3;

    UnitDepartment dep1;
    UnitDepartment dep2;
    UnitDepartment dep3;

    Manufacturer manuf1;

    Batch batch1;
    Batch batch2;

    Unit unit1;
    Unit unit2;

    Transaction transaction1;
    Transaction transaction2;
    Transaction transaction3;

    Verification verification1;
    Verification verification2;
    Verification verification3;

    ImageData imageData1;

    Product product1;

    CustomerResponse customerResponse1;

    TableParam tableParam;

    @Before
    public void createEntities() {

        tableParam = new TableParam ();
        tableParam.setCurrentPage(1);
        tableParam.setPerPage(50);

        unit1 = TestEntitiesGenerator.getTestUnit(null);
        unit2 = TestEntitiesGenerator.getTestUnit(null);

        unit1 = em.persist(unit1);
        unit2 = em.persist(unit2);

        dep1 = TestEntitiesGenerator.getTestDepartment(null, unit1.getIdUnit());
        dep2 = TestEntitiesGenerator.getTestDepartment(null, unit2.getIdUnit());
        dep3 = TestEntitiesGenerator.getTestDepartment(null, unit2.getIdUnit());

        dep1 = em.persist(dep1);
        dep2 = em.persist(dep2);
        dep3 = em.persist(dep3);

        manuf1 = TestEntitiesGenerator.getTestManufacturer(null, unit1.getIdUnit());
        manuf1 = em.persist(manuf1);

        batch1 = TestEntitiesGenerator.getTestBatch(null, dep1.getIdDep());
        batch2 = TestEntitiesGenerator.getTestBatch(null, dep2.getIdDep());

        batch1 = em.persist(batch1);
        batch2 = em.persist(batch2);

        product1 = TestEntitiesGenerator.getTestProduct(null,dep1.getIdDep(), null, 1000L);

        item1 = TestEntitiesGenerator.getTestItem(null,product1.getIdProduct(), dep1.getIdDep(), batch1.getIdBatch(), "");
        item2 = TestEntitiesGenerator.getTestItem(null,product1.getIdProduct(), dep1.getIdDep(), batch1.getIdBatch(), "");
        item3 = TestEntitiesGenerator.getTestItem(null,product1.getIdProduct(), dep2.getIdDep(), batch2.getIdBatch(), "");


        item1 = em.persist(item1);
        item2 = em.persist(item2);
        item3 = em.persist(item3);

        transaction1 = TestEntitiesGenerator.getTestTransaction(null, dep1.getIdDep(), item1.getIdItem());
        transaction2 = TestEntitiesGenerator.getTestTransaction(null, dep1.getIdDep(), item1.getIdItem());
        transaction3 = TestEntitiesGenerator.getTestTransaction(null, dep2.getIdDep(), item2.getIdItem());

        transaction1 = em.persist(transaction1);
        transaction2 = em.persist(transaction2);
        transaction3 = em.persist(transaction3);

        verification1 = TestEntitiesGenerator.getTestVerification(null, item1.getIdItem());
        verification2 = TestEntitiesGenerator.getTestVerification(null, item2.getIdItem());
        verification3 = TestEntitiesGenerator.getTestVerification(null, item2.getIdItem());

        verification1 = em.persist(verification1);
        verification2 = em.persist(verification2);
        verification3 = em.persist(verification3);

        imageData1 = TestEntitiesGenerator.getTestImageData(null);
        em.persist(imageData1);

        customerResponse1 = TestEntitiesGenerator.getTestCustomerResponse(null, verification1.getIdVerification());
        em.persist(customerResponse1);

        em.flush();
    }
}
