package onlyoriginal.restapi.repository;


import onlyoriginal.restapi.PostgresDataJpaTest;
import onlyoriginal.restapi.dao.repo.ItemRepository;
import onlyoriginal.restapi.model.dto.tables.Filter;
import onlyoriginal.restapi.model.dto.tables.Search;
import onlyoriginal.restapi.model.dto.tables.SortOrder;
import onlyoriginal.restapi.model.dto.tables.TableParam;
import onlyoriginal.restapi.model.entity.Item;
import onlyoriginal.restapi.model.enums.SortDirection;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;


@RunWith(SpringRunner.class)
@PostgresDataJpaTest
public class ItemDaoTest extends GenericRepositoryTest {

    @Autowired
    private ItemRepository itemRepository;


    @Test
    public void mustReturnCorrectEntityOnSearchById() {
        Item found = itemRepository.findByIdItem(item1.getIdItem());
        assertEquals(found, item1);
    }

    @Test
    public void mustReturnNullOnEmptyEntity() {
        Item found = itemRepository.findByIdItem(999L);
        assertNull(found);
    }

    @Test
    public void mustReturnCorrectEntityOnSearchByCodeItem() {
        Item found = itemRepository.findByCodeItem(item1.getCodeItem());
        assertEquals(found, item1);
    }


    @Test
    public void mustReturnCorrectEntityListOnSearchAllEntities() {
        List<Item> foundList = itemRepository.findAll();
        assertEquals(foundList.size(), 3);
        assertTrue(foundList.contains(item1));
        assertTrue(foundList.contains(item2));
        assertTrue(foundList.contains(item3));
    }

    @Test
    public void mustReturnCorrectEntityListOnSearchAllByIdDep() {
        List<Item> foundList = itemRepository.findAllByIdDep(dep1);
        assertEquals(foundList.size(), 2);
        assertTrue(foundList.contains(item1));
        assertTrue(foundList.contains(item2));
    }

    @Test
    public void mustReturnEmptyListWhenSearchByIdDepAndNotFound() {
        List<Item> foundList = itemRepository.findAllByIdDep(dep3);
        assertNotNull(foundList);
        assertEquals(foundList.size(), 0);
    }

    @Test
    public void mustReturnCorrectEntityListOnSearchByIdUnit() {
        List<Item> foundList = itemRepository.findAllByIdUnit(tableParam, unit1.getIdUnit()).getContent();
        assertEquals(foundList.size(), 2);
        assertTrue(foundList.contains(item1));
        assertTrue(foundList.contains(item2));
    }
}