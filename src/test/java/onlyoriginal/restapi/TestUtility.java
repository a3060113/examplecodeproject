package onlyoriginal.restapi;

import onlyoriginal.restapi.model.dto.*;
import onlyoriginal.restapi.utils.DateUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static onlyoriginal.restapi.controller.GenericControllerTest.isLong;
import static onlyoriginal.restapi.controller.GenericControllerTest.isUUID;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class TestUtility {

    public static final Map<Class<?>, DTOBodyTester> CHECKERS_FOR_DTO;

    static {
        Map<Class<?>, DTOBodyTester> checkers = new HashMap<>();

        DTOBodyTester<ItemDTO> itemDTOChecker = (actions, dto) -> {
            actions.andExpect(jsonPath("$.idItem", isLong(dto.getIdItem())))
                    .andExpect(jsonPath("$.codeItem", isUUID(dto.getCodeItem())))
                    .andExpect(jsonPath("$.name", is(dto.getName())))
                    .andExpect(jsonPath("$.countryOrigin", is(dto.getCountryOrigin().toString())))
                    .andExpect(jsonPath("$.idDep", isLong(dto.getIdDep())))
                    .andExpect(jsonPath("$.idBatch", isLong(dto.getIdBatch())))
                    .andExpect(jsonPath("$.info.name", is(dto.getInfo().get("name"))))
                    .andExpect(jsonPath("$.manufactured", is(DateUtils.localDateToString(
                            dto.getManufactured(), DateUtils.YYYY_MM_DD))))
                    .andExpect(jsonPath("$.images[0]", isLong(dto.getImages().get(0))));
        };
        checkers.put(ItemDTO.class, itemDTOChecker);

        CHECKERS_FOR_DTO = Collections.unmodifiableMap(checkers);

    }
}
