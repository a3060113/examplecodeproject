package onlyoriginal.restapi.controller.web;


import onlyoriginal.restapi.config.properties.ConfigProperties;
import onlyoriginal.restapi.controller.GenericControllerTest;
import onlyoriginal.restapi.dao.repo.ImageDataRepository;
import onlyoriginal.restapi.dao.repo.ProductRepository;
import onlyoriginal.restapi.model.dto.ItemDTO;
import onlyoriginal.restapi.service.DecoderService;
import onlyoriginal.restapi.service.ItemService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static onlyoriginal.restapi.utils.TestDtoGenerator.getTestItemDTO;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@WebMvcTest({ItemController.class})
public class ItemControllerTest extends GenericControllerTest {

    @MockBean
    private ItemService service;

    @MockBean
    private DecoderService decoderService;

    @MockBean
    private ConfigProperties configProperties;

    @MockBean
    private ImageDataRepository repo;

    @MockBean
    private ProductRepository productRepository;

    private ItemDTO itemDTO;
    private ItemDTO itemDTO2;
    private ItemDTO itemDTO3;

    private List<ItemDTO> items;
    private LocalDate date;

    @Before
    public void createItem() {

        itemDTO = getTestItemDTO(10L, 10L, 10L, Arrays.asList(10L, 20L));
        itemDTO2 = getTestItemDTO(20L, 10L, 10L, Arrays.asList(10L, 20L));
        itemDTO3 = getTestItemDTO(30L, 10L, 10L, Arrays.asList(10L, 20L));
        date = itemDTO.getManufactured();
        items = Arrays.asList(itemDTO, itemDTO2, itemDTO3);
    }

    @Test
    public void mustReturnCorrectDTOOnSearchByIdRequest() throws Exception {
        given(service.getById(10L)).willReturn(itemDTO);
        String content = objectMapper.writeValueAsString(itemDTO);
        checkReturnSingleBody("/web/item/10", HttpMethod.GET, content, itemDTO);
    }

    @Test
    public void mustReturn404OnSearchByIdAndNotFound() throws Exception {
        given(service.getById(100000L)).willReturn(null);
        String content = objectMapper.writeValueAsString(itemDTO);
        checkNotFoundReturnStatus("/web/item/10", HttpMethod.GET, content);
    }

    @Test
    public void mustReturnCorrectDTOOnSearchByCodeRequest() throws Exception {
        given(service.getItemByCode(itemDTO.getCodeItem())).willReturn(itemDTO);
        String content = objectMapper.writeValueAsString(itemDTO);
        checkReturnSingleBody("/web/item/get-by-code/" + itemDTO.getCodeItem(), HttpMethod.GET, content, itemDTO);
    }

    @Test
    public void mustReturnCorrectDTOOnCreateRequest() throws Exception {
        given(service.create(itemDTO)).willReturn(itemDTO);
        String content = objectMapper.writeValueAsString(itemDTO);
        checkReturnSingleBody("/web/item", HttpMethod.POST, content, itemDTO);
    }

    @Test
    public void mustReturnCorrectDTOOnUpdateRequest() throws Exception {
        given(service.update(itemDTO)).willReturn(itemDTO);
        String content = objectMapper.writeValueAsString(itemDTO);
        checkReturnSingleBody("/web/item", HttpMethod.PUT, content, itemDTO);
    }

/*    @Test
    public void mustReturnCorrectDTOListOnSearchAllRequest() throws Exception {
        given(service.findAll()).willReturn(items);
        String content = objectMapper.writeValueAsString(items);
        checkReturnArray("/web/item/all", HttpMethod.GET, content, 3);
    }*/

    @Test
    public void mustReturnCorrectDTOListOnSearchByIdDepRequest() throws Exception {
        given(service.findAllByIdDep(10L)).willReturn(items);
        String content = objectMapper.writeValueAsString(items);
        checkReturnArray("/web/item/all-by-department/10", HttpMethod.GET, content, 3);
    }

    @Test
    public void mustReturnCorrectDTOListOnSearchByIdUnitRequest() throws Exception {
        given(service.findAllByIdUnit(10L)).willReturn(items);
        String content = objectMapper.writeValueAsString(items);
        checkReturnArray("/web/item/all-by-unit/10", HttpMethod.GET, content, 3);
    }

    @Test
    public void mustReturnOkStatusOnDeleteRequest() throws Exception {
        checkOkReturnStatus("/web/item/10", HttpMethod.DELETE);
    }
}