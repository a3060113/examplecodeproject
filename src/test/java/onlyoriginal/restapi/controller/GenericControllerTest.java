package onlyoriginal.restapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import onlyoriginal.restapi.TestUtility;
import org.hamcrest.Matcher;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class GenericControllerTest extends Assert {

    @Autowired
    protected MockMvc mvc;

    @MockBean
    protected HttpHeaders httpHeaders;

    protected ObjectMapper objectMapper = new ObjectMapper();

    protected void checkOkReturnStatus(String path, HttpMethod method) throws Exception {
        mvc.perform(forMethod(method, path).with(getAdminCredentials())
                .with(csrf())
                .characterEncoding("utf-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private MockHttpServletRequestBuilder forMethod(HttpMethod method, String path) {
        if (method == HttpMethod.POST) {
            return post(path);
        } else if (method == HttpMethod.PUT) {
            return put(path);
        } else if (method == HttpMethod.DELETE) {
            return delete(path);
        } else {
            return get(path);
        }
    }

    private SecurityMockMvcRequestPostProcessors.UserRequestPostProcessor getAdminCredentials() {
        return user("Admin").password("123456").roles("ADMIN", "USER", "MOBILE");
    }

    public static Matcher<Integer> isLong(Long value) {
        return org.hamcrest.core.Is.is(Integer.parseInt(value.toString()));
    }

    public static Matcher<String> isUUID(UUID value) {
        return org.hamcrest.core.Is.is(value.toString());
    }

    protected void checkNotFoundReturnStatus(String path, HttpMethod method, String content) throws Exception {
        mvc.perform(forMethod(method, path).with(getAdminCredentials())
                .with(csrf())
                .characterEncoding("utf-8")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    protected ResultActions checkReturnStatusAndEncoding(String path, HttpMethod method, String content) throws Exception {
        return mvc.perform(forMethod(method, path)
                .with(getAdminCredentials())
                .with(csrf())
                .content(content)
                .characterEncoding("utf-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private ResultActions checkNegativeReturnStatusAndEncoding(String path, HttpMethod method, String content) throws Exception {
        return mvc.perform(forMethod(method, path)
                .with(getAdminCredentials())
                .with(csrf())
                .content(content)
                .characterEncoding("utf-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    protected void checkReturnArray(String path, HttpMethod method, String content, int size) throws Exception {
        checkReturnStatusAndEncoding(path, method, content).andExpect(jsonPath("$", hasSize(size)));
    }

    protected void checkPageReturn(String path, HttpMethod method, String content, int size) throws Exception {
        checkReturnStatusAndEncoding(path, method, content).andExpect(jsonPath("$.content", hasSize(size)));
    }

    protected <T> void checkReturnSingleBody(String path, HttpMethod method, String content, T dto) throws Exception {
        ResultActions actions = checkReturnStatusAndEncoding(path, method, content);
        TestUtility.CHECKERS_FOR_DTO.get(dto.getClass()).testDTO(actions, dto);
    }

    protected <T> void checkReturnSingleBodyNegative(String path, HttpMethod method, String content, T dto) throws Exception {
        checkNegativeReturnStatusAndEncoding(path, method, content);

    }

}