package onlyoriginal.restapi;

import org.springframework.test.web.servlet.ResultActions;

@FunctionalInterface
public interface DTOBodyTester<T> {
    void testDTO (ResultActions actions, T dto) throws Exception;
}
