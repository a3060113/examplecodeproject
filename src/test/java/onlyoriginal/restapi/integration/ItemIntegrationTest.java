package onlyoriginal.restapi.integration;


import onlyoriginal.restapi.RestApiRunner;
import onlyoriginal.restapi.dao.repo.ProductRepository;
import onlyoriginal.restapi.dao.repo.UnitDepartmentRepository;
import onlyoriginal.restapi.dao.repo.UnitRepository;
import onlyoriginal.restapi.model.dto.ItemDTO;
import onlyoriginal.restapi.model.dto.UnitDTO;
import onlyoriginal.restapi.model.dto.UnitDepartmentDTO;
import onlyoriginal.restapi.model.dto.tables.Filter;
import onlyoriginal.restapi.model.dto.tables.Search;
import onlyoriginal.restapi.model.dto.tables.SortOrder;
import onlyoriginal.restapi.model.dto.tables.TableParam;
import onlyoriginal.restapi.model.entity.Product;
import onlyoriginal.restapi.model.entity.Unit;
import onlyoriginal.restapi.model.entity.UnitDepartment;
import onlyoriginal.restapi.model.enums.CountryOrigin;
import onlyoriginal.restapi.model.enums.SortDirection;
import onlyoriginal.restapi.service.DecoderService;
import onlyoriginal.restapi.service.ItemService;
import onlyoriginal.restapi.service.UnitDepartmentService;
import onlyoriginal.restapi.service.UnitService;
import onlyoriginal.restapi.utils.TestDtoGenerator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static onlyoriginal.restapi.utils.TestDtoGenerator.getTestDepartmentDTO;
import static onlyoriginal.restapi.utils.TestDtoGenerator.getTestUnitDTO;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RestApiRunner.class)
@AutoConfigureMockMvc
public class ItemIntegrationTest extends GenericIntegrationTest {

    @Autowired
    private ItemService itemService;

    @Autowired
    private UnitDepartmentService unitDepartmentService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private DecoderService decoderService;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UnitDepartmentRepository departmentRepository;

    @Autowired
    private UnitRepository unitRepository;


    @Before
    public void before2() throws Exception {

        initDBStrucure();
    }

    @Sql("/truncate.sql")
    @Test
    public void generateItemFromProduct() throws Exception {

        if (itemService.findAll().size() > 1) {
            throw new Exception("DB Not CLEAR !!! " + itemService.findAll().size());
        }

        checkReturnSingleBody("/web/product", HttpMethod.POST, objectMapper.writeValueAsString(productDTO), productDTO);

        checkOkReturnStatus("/web/item/generate-from-product/1000", HttpMethod.GET);

        ItemDTO itemP = itemService.getById(1001L);

        Assert.assertEquals(productDTO.getInfo(), itemP.getInfo());

    }

    @Sql("/truncate.sql")
    @Test
    public void getItemByTableParamComplexCheck() throws Exception {

        ItemDTO itemDTO1 = TestDtoGenerator.getTestItemDTO(1001L, 1000L, 1000L, imageDataIdList);
        ItemDTO itemDTO2 = TestDtoGenerator.getTestItemDTO(1002L, 1000L, 1000L, imageDataIdList);
        ItemDTO itemDTO3 = TestDtoGenerator.getTestItemDTO(1003L, 1000L, 1000L, imageDataIdList);
        ItemDTO itemDTO4 = TestDtoGenerator.getTestItemDTO(1004L, 1000L, 1000L, imageDataIdList);

        TableParam tableParam = new TableParam();
        tableParam.setCurrentPage(1);
        tableParam.setPerPage(2);
        tableParam.setSortOrder(new SortOrder("idItem", SortDirection.ASC));
        tableParam.setFilters(Arrays.asList(new Filter("name", item.getName()),
                new Filter("countryOrigin", "UKRAINE")));
        tableParam.setSearch(new Search("", item.getName().substring(1, item.getName().length() - 2)));

        itemDTO1.setName("abrvalg");
        itemDTO2.setName("1123");
        itemDTO3.setName(item.getName());
        itemDTO4.setName("1123");

        itemService.create(itemDTO1);
        itemService.create(itemDTO2);
        itemService.create(itemDTO3);
        itemService.create(itemDTO4);

        List<ItemDTO> itemList = itemService.findAll(tableParam).getContent();

        assertEquals(2, itemList.size());
        assertEquals(item.getIdItem(), itemList.get(0).getIdItem());
        assertEquals(itemDTO3.getIdItem(), itemList.get(1).getIdItem());
        assertEquals(itemList.get(0).getImages().get(0).toString(), "1000");

        String base64Encoded = decoderService.encodeTableParamToBase64(tableParam);

        checkPageReturn("/web/item/table/all/" + base64Encoded, HttpMethod.GET, "", 2);
        checkPageReturn("/web/item/table/all-by-unit/" + base64Encoded, HttpMethod.GET, "", 2);

    }


    @Sql("/truncate.sql")
    @Test
    public void getItemByTableParamNotForSearchCheck() throws Exception {

        imageDataIdList = new ArrayList<>();
        imageDataIdList.add(1066L);

        ItemDTO itemDTO1 = TestDtoGenerator.getTestItemDTO(1001L, 1000L, 1000L, imageDataIdList);
        ItemDTO itemDTO2 = TestDtoGenerator.getTestItemDTO(1002L, 1000L, 1000L, imageDataIdList);
        ItemDTO itemDTO3 = TestDtoGenerator.getTestItemDTO(1003L, 1000L, 1000L, imageDataIdList);
        ItemDTO itemDTO4 = TestDtoGenerator.getTestItemDTO(1004L, 1000L, 1000L, imageDataIdList);

        TableParam tableParam = new TableParam();
        tableParam.setCurrentPage(1);
        tableParam.setPerPage(2);
        tableParam.setSortOrder(new SortOrder("idItem", SortDirection.ASC));
        tableParam.setFilters(Arrays.asList(new Filter("name", item.getName()),
                new Filter("countryOrigin", "UKRAINE")));
        tableParam.setSearch(new Search("", "66"));

        itemDTO1.setName("abrvalg");
        itemDTO2.setName("1123");
        itemDTO3.setName(item.getName());
        itemDTO4.setName("1123");

        itemService.create(itemDTO1);
        itemService.create(itemDTO2);
        itemService.create(itemDTO3);
        itemService.create(itemDTO4);

        List<ItemDTO> itemList = itemService.findAll(tableParam).getContent();

        assertEquals(0, itemList.size());

    }

    @Sql("/truncate.sql")
    @Test
    public void getItemsByUnitId() throws Exception {

        ItemDTO itemDTO1 = TestDtoGenerator.getTestItemDTO(1001L, 1000L, 1000L, imageDataIdList);
        ItemDTO itemDTO2 = TestDtoGenerator.getTestItemDTO(1002L, 1000L, 1000L, imageDataIdList);
        ItemDTO itemDTO3 = TestDtoGenerator.getTestItemDTO(1003L, 1001L, 1000L, imageDataIdList);
        ItemDTO itemDTO4 = TestDtoGenerator.getTestItemDTO(1004L, 1001L, 1000L, imageDataIdList);

        TableParam tableParam = new TableParam();
        tableParam.setCurrentPage(1);
        tableParam.setPerPage(10);
        tableParam.setSortOrder(new SortOrder("idItem", SortDirection.ASC));
        tableParam.setFilters(Arrays.asList(new Filter("idDep", itemDTO3.getIdDep().toString())));
        tableParam.setSearch(new Search("", itemDTO3.getName().substring(1, item.getName().length() - 2)));

        UnitDepartmentDTO unitDep1 = getTestDepartmentDTO(1001L, 1001L);
        UnitDTO unit1 = getTestUnitDTO(1001L);

        unitService.create(unit1);
        unitDepartmentService.create(unitDep1);
        itemService.create(itemDTO1);
        itemService.create(itemDTO2);
        itemService.create(itemDTO3);
        itemService.create(itemDTO4);

        List<ItemDTO> itemList = itemService.findAllByIdUnit(tableParam, 1001L).getContent();

        assertEquals(2, itemList.size());
        assertEquals(itemList.get(0).getIdItem(), itemDTO3.getIdItem());
        assertEquals(itemList.get(0).getImages().get(0), imageData.getIdImage());
    }

    @Sql("/truncate.sql")
    @Test
    public void barcodeMustBePresentOnGetById() throws Exception {
        String barcode = "999";
        Unit unit = unitRepository.save(new Unit(null, "UnitName", "JurAddress", "FactAddress", "123456789", LocalDate.now(), 88, "infoUnit"));
        UnitDepartment department = departmentRepository.save(new UnitDepartment(44L, unit, "somethingMeaningful", "somethingMeaningful", "somethingMeaningful", "somethingMeaningful"));
        Product product = productRepository.save(new Product(1L, "productz", CountryOrigin.UKRAINE, department, "someInfoItem", "someImages", barcode, 1000L));
        ItemDTO itemDTO1 = TestDtoGenerator.getTestItemDTO(1001L, department.getIdDep(), 1000L, imageDataIdList);
        itemDTO1.setIdProduct(product.getIdProduct());
        ItemDTO createdItem = itemService.create(itemDTO1);

        checkReturnStatusAndEncoding("/web/item/" + createdItem.getIdItem(), HttpMethod.GET, "")
                .andExpect(jsonPath("$.barcode", is(barcode)));


    }


}
