package onlyoriginal.restapi.integration;

import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import onlyoriginal.restapi.controller.GenericControllerTest;
import onlyoriginal.restapi.model.dto.ItemDTO;
import onlyoriginal.restapi.model.enums.TypeTransaction;
import org.junit.Before;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.TestPropertySource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static onlyoriginal.restapi.utils.TestDtoGenerator.*;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@AutoConfigureTestDatabase(replace = NONE)
@AutoConfigureEmbeddedDatabase
@TestPropertySource(properties = {
         "defUnit=1000",
         "logging.level.org.hibernate.type=TRACE"
        } )
public class GenericIntegrationTest extends GenericControllerTest {


    protected UnitDTO unit;
    protected UnitDepartmentDTO unitDep;
    protected BatchDTO batch;
    protected ItemDTO item;
    protected ImageDataDTO imageData;
    protected List<Long> imageDataIdList;
    protected TransactionDTO transactionManufact;
    protected TransactionDTO transactionDismiss;
    protected CheckForOriginDTO checkForOrigin;
    protected ItemInfoDTO itemInfo;
    protected CustomerRequestDTO customerRequestDTO;
    protected CustomerResponseDTO customerResponseDTO;
    protected VerificationDTO verificationDTO;
    protected ProductDTO productDTO;

    @Before
    public void prepareData() {
        unit = getTestUnitDTO(1000L);
        unitDep = getTestDepartmentDTO(1000L, 1000L);
        item = getTestItemDTO(1000L, 1000L, 1000L, Arrays.asList(1000L));
        imageData = getTestImageDataDTO(1000L);
        imageDataIdList = Arrays.asList(1000L);
        batch = getTestBatchDTO(1000L, 1000L);
        productDTO = getTestProductDTO(1000L, 1000L, 1000L, Arrays.asList(1000L) );

        transactionManufact = getTestTransactionDTO(1000L, 1000L, 1000L);
        transactionDismiss = getTestTransactionDTO(1001L, 1000L, 1000L);

        itemInfo = new ItemInfoDTO(item, unit, unitDep, batch, 1000L, new ArrayList<>());
        checkForOrigin = getCheckForOriginDTO(1000L);
        customerRequestDTO = getCustomerRequestDTO(1000L);
        verificationDTO = getTestVerificationDTO(1000L, 1000L);

        customerResponseDTO = getCustomerResponseDTO(null, 1001L, imageDataIdList);
    }

    protected void initDBStrucure() throws Exception {
        checkReturnSingleBody("/web/unit", HttpMethod.POST, objectMapper.writeValueAsString(unit), unit);
        checkReturnSingleBody("/web/department", HttpMethod.POST, objectMapper.writeValueAsString(unitDep), unitDep);
        checkReturnSingleBody("/web/batch", HttpMethod.POST, objectMapper.writeValueAsString(batch), batch);
        checkReturnSingleBody("/mobile/image", HttpMethod.POST, objectMapper.writeValueAsString(imageData), imageData);
        checkReturnSingleBody("/web/item", HttpMethod.POST, objectMapper.writeValueAsString(item), item);
    }

    protected void initTransactions() throws Exception {
        transactionManufact.setTypeTransaction(TypeTransaction.MANUFACTURE);
        transactionDismiss.setTypeTransaction(TypeTransaction.DISMISSING);

        checkReturnSingleBody("/web/transaction", HttpMethod.POST, objectMapper.writeValueAsString(transactionManufact), transactionManufact);
        checkReturnSingleBody("/web/transaction", HttpMethod.POST, objectMapper.writeValueAsString(transactionDismiss), transactionDismiss);
    }
}
