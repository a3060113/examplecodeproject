package onlyoriginal.restapi.service;

import onlyoriginal.restapi.dao.repo.ImageDataRepository;
import onlyoriginal.restapi.dao.repo.ProductRepository;
import onlyoriginal.restapi.model.dto.UnitDTO;
import onlyoriginal.restapi.model.dto.converter.UnitToDTOConverter;
import onlyoriginal.restapi.model.entity.Unit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@WebMvcTest(ConverterService.class)
public class ConverterServiceTest {

    @Autowired
    private ConverterService service;

    @Autowired
    private UnitToDTOConverter converter;

    @MockBean
    private ItemImageService itemImageService;

    @MockBean
    private ImageDataRepository repo;

    @MockBean
    private ProductRepository productRepository;

    private Unit unit1;
    private Unit unit2;
    private Unit unit3;
    private List<Unit> units;
    private UnitDTO unitDto1;
    private UnitDTO unitDto2;
    private UnitDTO unitDto3;
    private List<UnitDTO> unitsDTO;

    @Before
    public void createTestData() {
        unit1 = new Unit(1L, "Unit name 1", "jur Address 1", "fact address 1", "gpsUnit 1", LocalDate.now(), 10, "unit1 info 1");
        unit2 = new Unit(2L, "Unit name 2", "jur Address 2", "fact address 2", "gpsUnit 2", LocalDate.now(), 10, "unit2 info 2");
        unit3 = new Unit(3L, "Unit name 3", "jur Address 3", "fact address 3", "gpsUnit 3", LocalDate.now(), 10, "unit3 info 3");
        units = Arrays.asList(unit1, unit2, unit3);

        unitDto1 = converter.convert(unit1);
        unitDto2 = converter.convert(unit2);
        unitDto3 = converter.convert(unit3);
        unitsDTO = Arrays.asList(unitDto1, unitDto2, unitDto3);
    }

    @Test
    public void mustReturnCorrectDtoOnConvertEntityRequest() {
        UnitDTO converted = service.convertEntityToDto(unit1);
        assertEquals(converted, unitDto1);
    }

    @Test
    public void mustReturnNullWhenEntityIsNull() {
        UnitDTO converted = service.convertEntityToDto(null);
        assertNull(converted);
    }

    @Test
    public void mustReturnCorrectEntityOnConvertFromDtoRequest() {
        Unit converted = service.convertEntityFromDto(unitDto1);
        assertEquals(converted, unit1);
    }

    @Test
    public void mustReturnCorrectDtoListOnConvertRequest() {
        List<UnitDTO> converted = service.convertEntitiesList(units);
        assertEquals(converted.size(), 3);
        assertEquals(converted, unitsDTO);
    }
}