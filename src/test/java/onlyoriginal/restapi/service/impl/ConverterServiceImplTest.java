package onlyoriginal.restapi.service.impl;

import onlyoriginal.restapi.dao.repo.ImageDataRepository;
import onlyoriginal.restapi.dao.repo.ProductRepository;
import onlyoriginal.restapi.model.dto.UnitDTO;
import onlyoriginal.restapi.model.dto.converter.UnitToDTOConverter;
import onlyoriginal.restapi.model.entity.Unit;
import onlyoriginal.restapi.service.ConverterService;
import onlyoriginal.restapi.service.ItemImageService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@WebMvcTest(ConverterService.class)
public class ConverterServiceImplTest {

    @Autowired
    private ConverterService service;

    @MockBean
    private ItemImageService itemImageService;

    @MockBean
    private ImageDataRepository repo;

    @MockBean
    private ProductRepository productRepository;

    @Autowired
    private UnitToDTOConverter converter;

    private Unit unit;
    private UnitDTO unitDto;

    @Before
    public void createTestData() {
        unit = new Unit(1L, "Unit name 1", "jur Address 1", "fact address 1", "gpsUnit 1", LocalDate.now(), 10, "unit info 1");
        unitDto = converter.convert(unit);
    }

    @Test
    public void convertUnitToDto (){
        UnitDTO converted = service.convertEntityToDto(unit);
        assertEquals(converted, unitDto);
    }

    @Test
    public void convertUnitFromDto (){
        Unit converted = service.convertEntityFromDto(unitDto);
        assertEquals(converted, unit);
    }
}