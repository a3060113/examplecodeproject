package onlyoriginal.restapi.dao.repo.custom.impl;

import lombok.AllArgsConstructor;
import onlyoriginal.restapi.dao.repo.custom.ItemRepositoryCustom;
import onlyoriginal.restapi.dao.repo.custom.root.ItemRepositoryRoot;
import onlyoriginal.restapi.model.dto.tables.TableParam;
import onlyoriginal.restapi.model.entity.Item;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

@Repository
@AllArgsConstructor
public class ItemRepositoryImpl extends GenericCustomRepository<Item> implements ItemRepositoryCustom {

    private final ItemRepositoryRoot root;

    @Override
    public Page<Item> findAll(TableParam tableParam) {
        return root.findAll(findAllByTableParam(
                tableParam, Item.class), tableParam.pageable());
    }

    @Override
    public Page<Item> findAllByIdUnit(TableParam tableParam, Long idUnit) {
        return root.findAll(findAllByTableParam(
                tableParam, Item.class).and(depToUnitJoin(idUnit)), tableParam.pageable());
    }
}
