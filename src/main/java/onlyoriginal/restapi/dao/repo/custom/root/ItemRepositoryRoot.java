package onlyoriginal.restapi.dao.repo.custom.root;

import onlyoriginal.restapi.model.entity.Item;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface ItemRepositoryRoot extends CrudRepository<Item, Long>, JpaSpecificationExecutor<Item> {

}
