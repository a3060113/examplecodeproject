package onlyoriginal.restapi.dao.repo.custom;

import onlyoriginal.restapi.model.dto.tables.TableParam;
import onlyoriginal.restapi.model.entity.Item;
import org.springframework.data.domain.Page;

public interface ItemRepositoryCustom {

    Page<Item> findAll(TableParam tableParam);

    Page<Item> findAllByIdUnit(TableParam tableParam, Long idUnit);

}
