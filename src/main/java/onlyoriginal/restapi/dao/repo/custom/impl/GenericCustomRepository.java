package onlyoriginal.restapi.dao.repo.custom.impl;


import lombok.AllArgsConstructor;
import lombok.Data;
import onlyoriginal.restapi.model.dto.tables.*;
import onlyoriginal.restapi.model.entity.Item;
import onlyoriginal.restapi.model.entity.Unit;
import onlyoriginal.restapi.model.entity.UnitDepartment;

import onlyoriginal.restapi.model.enums.SortDirection;
import onlyoriginal.restapi.utils.DateUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.criteria.*;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;


@Repository
public class GenericCustomRepository<T> {


    public static final String ID_DEP = "idDep";
    public static final String ID_UNIT = "idUnit";
    public static final String ID_ITEM = "idItem";

    Specification<T> depFilter(Long idDep) {
        return (Specification<T>) (root, criteriaQuery, builder)
                -> builder.equal(root.get(ID_DEP), idDep);
    }

    Specification<T> itemFilter(Long idItem) {
        return (Specification<T>) (root, criteriaQuery, builder)
                -> builder.equal(root.get(ID_ITEM), idItem);
    }

    Specification<T> depToUnitJoin(Long idUnit) {
        return (Specification<T>) (root, criteriaQuery, builder) -> {

            Join<T, UnitDepartment> depJoin = root.join(ID_DEP);
            Join<UnitDepartment, Unit> unitJoin = depJoin.join(ID_UNIT);
            return builder.equal(unitJoin.get(ID_UNIT), idUnit);
        };
    }

    Specification<T> itemToUnitJoin(Long idUnit) {
        return (Specification<T>) (root, criteriaQuery, builder) -> {
            Join<T, Item> itemJoin = root.join(ID_ITEM);
            Join<Item, UnitDepartment> depJoin = itemJoin.join(ID_DEP);
            Join<UnitDepartment, Unit> unitJoin = depJoin.join(ID_UNIT);
            return builder.equal(unitJoin.get(ID_UNIT), idUnit);
        };
    }

    Specification<T> findAllByTableParam(TableParam tableParam, Class<T> clazz) {
        return (Specification<T>) (root, query, builder) -> {
            SortDirection sortDirection = tableParam.getSortOrder() == null ? SortDirection.ASC : tableParam.getSortOrder().getDirection();
            List<Filter> filterList = tableParam.getFilters();
            Search search = tableParam.getSearch();
            DateSearch dateSearch = tableParam.getDateSearch();

            List<Field> allFieldTypes = Arrays.stream(clazz.getDeclaredFields()).collect(Collectors.toList());
            Map<String, FieldObject<T>> rootMap = getRootMap(root, allFieldTypes, clazz);
            List<String> allFields = allFieldTypes.stream().map(Field::getName).collect(Collectors.toList());

            if (tableParam.getSortOrder() == null) {
                String sortField = allFields.get(0);
                if (sortDirection == SortDirection.ASC) {
                    query.orderBy(builder.asc(root.get(sortField)));
                } else {
                    query.orderBy(builder.desc(root.get(sortField)));
                }
            }

            Predicate predicate = builder.conjunction();
            Predicate predicate2 = builder.disjunction();

            if (filterList != null) {
                for (Filter filter : filterList) {
                    predicate = builder.and(predicate, builder.equal(getField(rootMap, filter.getField()),
                            getValue(rootMap, filter.getField(), filter.getValue())));
                }
            }

            if (search != null) {
                String searchValue = "%" + search.getQuery().trim().toLowerCase() + "%";

                if (search.getField() != null && !search.getField().equals("")) {
                    Expression<String> searchKey = builder.lower(getField(rootMap, search.getField()).as(String.class));
                    predicate = builder.and(predicate, builder.like(searchKey, searchValue));
                } else {
                    for (String field : allFields) {
                        Expression<String> searchKey = builder.lower(getField(rootMap, field).as(String.class));
                        predicate2 = builder.or(predicate2, builder.like(searchKey, searchValue));
                    }
                    predicate = builder.and(predicate, predicate2);
                }
            }

            if (dateSearch != null && dateSearch.getField() != null) {
                String dateField = dateSearch.getField();
                LocalDateTime from = getLocalDateTime(dateSearch.getFrom(), LocalTime.MIN);
                LocalDateTime to = getLocalDateTime(dateSearch.getTo(), LocalTime.MAX);
                if (from != null && to != null) {
                    predicate = builder.and(predicate, builder.between(root.get(dateField), from, to));
                } else if (to != null) {
                    predicate = builder.and(predicate, builder.lessThanOrEqualTo(root.get(dateField), to));
                } else if (from != null) {
                    predicate = builder.and(predicate, builder.greaterThanOrEqualTo(root.get(dateField), from));
                }
            }

            query.where(predicate);
            return predicate;
        };
    }


    private LocalDateTime getLocalDateTime(String date, LocalTime localTime) {
        return (date == null) || (date == "") ? null
                : LocalDateTime.of(DateUtils.stringToLocalDate(date, DateUtils.YYYY_MM_DD), localTime);
    }

    private Path<T> getField(Map<String, FieldObject<T>> rootMap, String field) {
        FieldObject<T> fieldObject = rootMap.get(field);
        if (fieldObject == null) {
            throw new RuntimeException(" Field '" + field + "' - Not Present in DB table");
        }
        return rootMap.get(field).getPath();
    }

    private Object getValue(Map<String, FieldObject<T>> rootMap, String field, String value) {
        FieldObject<T> fieldObject = rootMap.get(field);
        if (fieldObject == null) {
            throw new RuntimeException(" Field '" + field + "' - Not Present in DB table");
        }

        Object valueO;
        if (fieldObject.isEnumField()) {
            valueO = Enum.valueOf(fieldObject.getField().getType().asSubclass(Enum.class), value);
        } else {
            valueO = value;
        }
        return valueO;
    }

    private Map<String, FieldObject<T>> getRootMap(Root<T> root, List<Field> fields, Class<T> clazz) {

        Map<String, FieldObject<T>> rootMap = new HashMap<>();
        List<Field> fieldsCopy = new ArrayList<>(fields);

        for (Field field : fieldsCopy) {
            try {
                boolean notForSearch = field.isAnnotationPresent(NotForSearch.class);
                boolean joinPresent = field.isAnnotationPresent(JoinColumn.class);
                boolean enumType = field.getType().isEnum();

                if (notForSearch) {
                    fields.remove(field);
                    continue;
                }

                if (joinPresent) {
                    String idField = Arrays.stream(field.getType().getDeclaredFields())
                            .filter(f -> f.getDeclaredAnnotationsByType(Id.class).length > 0)
                            .map(Field::getName)
                            .findFirst().get();
                    rootMap.put(field.getName(), new FieldObject<>(root.get(field.getName()).get(idField), false, field));
                } else {
                    rootMap.put(field.getName(), new FieldObject<>(root.get(field.getName()), enumType, field));
                }
            } catch (IllegalArgumentException e) {
                fields.remove(field);
            }
        }
        return rootMap;
    }

    @Data
    @AllArgsConstructor
    private class FieldObject<T> {
        private Path<T> path;
        private boolean enumField;
        private Field field;
    }


}
