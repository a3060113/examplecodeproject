package onlyoriginal.restapi.dao.repo;


import onlyoriginal.restapi.dao.repo.custom.ItemRepositoryCustom;
import onlyoriginal.restapi.model.entity.Item;
import onlyoriginal.restapi.model.entity.UnitDepartment;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface ItemRepository extends CrudRepository<Item, Long>, JpaSpecificationExecutor<Item>, ItemRepositoryCustom {

    Item findByIdItem(Long idItem);

    Item findByCodeItem(UUID codeItem);

    List<Item> findAll();

}
