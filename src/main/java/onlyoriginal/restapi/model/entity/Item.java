package onlyoriginal.restapi.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import onlyoriginal.restapi.model.dto.tables.NotForSearch;
import onlyoriginal.restapi.model.enums.CountryOrigin;
import onlyoriginal.restapi.model.enums.ItemStatus;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

/**
 * Created by andrii on 7/12/18.
 */


@Entity
@Table(name = "item")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_item", unique = true, insertable = false, updatable = false)
    private Long idItem;

    @NotNull
    @Column(name = "codeItem")
    private UUID codeItem;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "item_status")
    private ItemStatus itemStatus;

    @NotBlank(message = "name can not be empty or blank")
    @Size(min = 3, message = "name must be longer than 3 digits")
    @Column(name = "name")
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "country_origin")
    private CountryOrigin countryOrigin;

    @ManyToOne
    @JoinColumn(name = "id_dep")
    private UnitDepartment idDep;

    @Column(name = "id_batch")
    private Long idBatch;

    @NotBlank(message = "infoItem can not be empty or blank")
    @Column(name = "info_item")
    private String infoItem;

    @NotNull
    @Column(name = "manufactured", updatable = false)
    private LocalDate manufactured;

    @Column(name = "id_product")
    private Long idProduct;

    @NotForSearch
    @Column(name = "images")
    private String images;

}
