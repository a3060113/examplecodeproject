package onlyoriginal.restapi.model.dto.tables;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;

@ToString
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TableParam {

    private Integer currentPage;
    private Integer perPage;
    private List<Filter> filters = new ArrayList<>();
    private Search search;
    private SortOrder sortOrder;
    private DateSearch dateSearch;

    public Pageable pageable() {

        if (currentPage == null) {
            currentPage = 1;
        }

        if (perPage == null) {
            perPage = 20;
        }

        Sort sort;
        if (sortOrder != null) {
            sort = new Sort(Sort.Direction.valueOf(sortOrder.getDirection().name()), sortOrder.getField());
            return PageRequest.of(currentPage - 1, perPage, sort);
        } else {
            return PageRequest.of(currentPage - 1, perPage);
        }
    }

    public void addFilter(Filter filter) {
        filters.add(filter);
    }

}