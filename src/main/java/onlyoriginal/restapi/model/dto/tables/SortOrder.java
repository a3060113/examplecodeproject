package onlyoriginal.restapi.model.dto.tables;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import onlyoriginal.restapi.model.enums.SortDirection;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SortOrder {

    private String field;
    private SortDirection direction;

}
