package onlyoriginal.restapi.model.dto.tables;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Search {

    private String field;
    private String query;
  //  private List<String> groupFields;

}
