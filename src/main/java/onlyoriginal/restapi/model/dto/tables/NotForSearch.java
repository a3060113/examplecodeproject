package onlyoriginal.restapi.model.dto.tables;


import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**

 This field will be excluded from TableSearch

 * */

@Target({FIELD})
@Retention(RUNTIME)
public @interface NotForSearch {

}
