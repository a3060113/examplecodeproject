package onlyoriginal.restapi.model.dto.tables;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DateSearch {

    private String field;
    private String from;
    private String to;

}
