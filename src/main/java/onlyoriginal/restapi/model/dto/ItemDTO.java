package onlyoriginal.restapi.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import onlyoriginal.restapi.model.enums.CountryOrigin;
import onlyoriginal.restapi.model.enums.ItemStatus;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemDTO {

    private Long idItem;

    @NotNull
    private UUID codeItem;

    @NotNull
    private ItemStatus itemStatus;

    @NotBlank(message = "name can not be empty or blank")
    @Size(min = 3, message = "name must be longer than 3 digits")
    private String name;

    @NotNull
    private CountryOrigin countryOrigin;

    @NotNull
    private Long idDep;

    private Long idProduct;

    private Long idBatch;

    @NotEmpty
    private Map<String, String> info;

    private List<Long> images;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate manufactured;

    private String barcode;

}
