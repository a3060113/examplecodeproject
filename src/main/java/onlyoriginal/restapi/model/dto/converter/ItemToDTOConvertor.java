package onlyoriginal.restapi.model.dto.converter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import onlyoriginal.restapi.dao.repo.ProductRepository;
import onlyoriginal.restapi.model.dto.ItemDTO;
import onlyoriginal.restapi.model.entity.Item;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by andrii on 7/29/18.
 */

@Component
@AllArgsConstructor
public class ItemToDTOConvertor implements Converter<Item, ItemDTO> {


    private final ObjectMapper objectMapper;

    private final ProductRepository productRepository;

    @Nullable
    @Override
    public ItemDTO convert(Item entity) {

        Map<String, String> infoMap;
        List<Long> images;
        try {
            infoMap = objectMapper.readValue(entity.getInfoItem(), new TypeReference<Map<String, String>>() {});
            images =  objectMapper.readValue(entity.getImages(), new TypeReference<List<Long>>() {});
        } catch (IOException e) {
            throw new RuntimeException(" Can not convert ItemEntity.info to Map ");
        }

        return new ItemDTO(
                        entity.getIdItem()
                        , entity.getCodeItem()
                        , entity.getItemStatus()
                        , entity.getName()
                        , entity.getCountryOrigin()
                        , entity.getIdDep().getIdDep()
                        , entity.getIdProduct()
                        , entity.getIdBatch()
                        , infoMap
                        , images
                        , entity.getManufactured()
                        , productRepository.findBarcodeByItemId(entity.getIdItem())
                );
    }
}