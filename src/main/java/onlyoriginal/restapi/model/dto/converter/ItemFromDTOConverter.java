package onlyoriginal.restapi.model.dto.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import onlyoriginal.restapi.model.dto.ItemDTO;
import onlyoriginal.restapi.model.entity.Item;
import onlyoriginal.restapi.model.entity.UnitDepartment;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;


@Component
@AllArgsConstructor
public class ItemFromDTOConverter implements Converter<ItemDTO, Item> {

    private final ObjectMapper objectMapper;

    @Nullable
    @Override
    public Item convert(ItemDTO dto) {
        try {
            return new Item(
                                dto.getIdItem()
                                , dto.getCodeItem()
                                , dto.getItemStatus()
                                , dto.getName()
                                , dto.getCountryOrigin()
                                , UnitDepartment.builder().idDep(dto.getIdDep()).build()
                                , dto.getIdBatch()
                                , objectMapper.writeValueAsString(dto.getInfo())
                                , dto.getManufactured()
                                , dto.getIdProduct()
                                , objectMapper.writeValueAsString(dto.getImages())
                        );
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Cannot convert to String ItemInfo map");
        }
    }

}
