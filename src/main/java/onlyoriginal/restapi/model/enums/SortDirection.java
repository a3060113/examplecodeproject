package onlyoriginal.restapi.model.enums;

public enum SortDirection {

    ASC, DESC

}
