package onlyoriginal.restapi.model.enums;

/**
 * Created by andrii on 7/12/18.
 */
public enum CountryOrigin {

    UKRAINE, ARMENIA, GEORGIA

}
