package onlyoriginal.restapi.model.enums;

public enum ItemStatus {
    MANUFACTURED,
    CANCELED,
    SHIPPED,
    RECEIVED
}
