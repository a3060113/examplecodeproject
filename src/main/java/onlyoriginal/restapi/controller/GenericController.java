package onlyoriginal.restapi.controller;


import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by andrii on 7/25/18.
 */


public class GenericController {

    protected <T> T buildResponse(T entity, HttpServletResponse response) {

        if (entity == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
        }
        return entity;
    }

    protected <T> Page<T> buildResponseForList(Page<T> entityList, HttpServletResponse response) {

        if (!entityList.hasContent()) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
        }
        return entityList;
    }

}
