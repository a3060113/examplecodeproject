package onlyoriginal.restapi.controller.web;

import lombok.AllArgsConstructor;
import onlyoriginal.restapi.controller.GenericController;
import onlyoriginal.restapi.model.dto.ItemDTO;
import onlyoriginal.restapi.service.DecoderService;
import onlyoriginal.restapi.service.ItemService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.UUID;


@RestController
@RequestMapping(value = "/web/item")
@AllArgsConstructor
public class ItemController extends GenericController {

    private final ItemService service;
    private final DecoderService decoderService;

    @GetMapping(value = "/{idItem}")
    public ItemDTO getItem(@PathVariable("idItem") Long id, HttpServletResponse response) {
        return buildResponse(service.getById(id), response);
    }

    @GetMapping(value = "/get-by-code/{code-item}")
    public ItemDTO getItem(@PathVariable("code-item") UUID codeItem, HttpServletResponse response) {
        return buildResponse(service.getItemByCode(codeItem), response);
    }

    @PutMapping
    public ItemDTO updateItem(@RequestBody @Valid ItemDTO dto, HttpServletResponse response) {
        return buildResponse(service.update(dto), response);
    }

    @PostMapping
    public ItemDTO createItem(@RequestBody @Valid ItemDTO dto, HttpServletResponse response) {
        return buildResponse(service.create(dto), response);
    }

    @GetMapping(value = "/table/all/{tableParam}")
    public Page<ItemDTO> getAllItems(HttpServletResponse response,
                                     @PathVariable("tableParam") String tableParam) {
        return buildResponseForList(service.findAll(
                decoderService.decodeTableParamFromBase64(tableParam)), response);
    }

}