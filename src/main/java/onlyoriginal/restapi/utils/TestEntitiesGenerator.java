package onlyoriginal.restapi.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import onlyoriginal.restapi.model.entity.*;
import onlyoriginal.restapi.model.enums.*;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TestEntitiesGenerator {

    public static Item getTestItem(Long idItem, Long idProduct, Long idDep, Long idBatch, String idImages) {
        return new Item(idItem, UUID.randomUUID(), ItemStatus.MANUFACTURED, "name123434 ", CountryOrigin.UKRAINE,
                UnitDepartment.builder().idDep(idDep).build(), idBatch,
                "info1", LocalDate.now(), idProduct, idImages);
    }

}
