package onlyoriginal.restapi.utils;

import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by andrii on 7/29/18.
 */
public class DateUtils {

    public static final String YYYY_MM_DD_HH_MI_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String YYYY_MM_DD = "yyyy-MM-dd";

    public static String timestampToString(Timestamp timestamp, String pattern){
        return timestamp.toLocalDateTime().format(DateTimeFormatter.ofPattern(pattern));
    }

    public static Timestamp getTimestampFromString(String date, String pattern) {
        return Timestamp.valueOf(LocalDateTime.parse(date, DateTimeFormatter.ofPattern(pattern)));
    }

    public static LocalDate stringToLocalDate(String date, String pattern){
        return LocalDate.parse(date, DateTimeFormatter.ofPattern(pattern));
    }

    public static String localDateToString(LocalDate date, String pattern){
        return date.format(DateTimeFormatter.ofPattern(pattern));
    }

    public static LocalDateTime stringToLocalDateTime(String date, String pattern){
        return LocalDateTime.parse(date, DateTimeFormatter.ofPattern(pattern));
    }

    public static String localDateTimeToString(LocalDateTime date, String pattern){

        if (date == null) {
            return null;
        }

        return date.format(DateTimeFormatter.ofPattern(pattern));
    }


}
