package onlyoriginal.restapi.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import onlyoriginal.restapi.model.dto.*;
import onlyoriginal.restapi.model.enums.*;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TestDtoGenerator {


    public static ItemDTO getTestItemDTO(Long idItem, Long idDep, Long idBatch, List<Long> idImages) {
        return new ItemDTO(idItem, UUID.randomUUID(), ItemStatus.MANUFACTURED, "name1", CountryOrigin.UKRAINE, idDep, null, idBatch,
                new HashMap<String, String>() {{
                    put("name", "item Name");
                    put("description", "item description");
                }},
                idImages,
                LocalDate.now(),
                null);
    }

}
