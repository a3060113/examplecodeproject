package onlyoriginal.restapi;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class RestApiRunner {

    public static void main(String[] args) {
        new SpringApplication(RestApiRunner.class).run(args);
    }

}
