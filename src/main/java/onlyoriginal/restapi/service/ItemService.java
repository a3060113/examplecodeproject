package onlyoriginal.restapi.service;

import onlyoriginal.restapi.model.dto.ItemDTO;
import onlyoriginal.restapi.model.dto.tables.TableParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;


public interface ItemService extends Crud<ItemDTO> {

    ItemDTO getItemByCode(UUID codeItem);

    Page<ItemDTO> findAll(TableParam tableParam);

}
