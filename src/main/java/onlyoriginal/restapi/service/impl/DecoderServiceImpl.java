package onlyoriginal.restapi.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import onlyoriginal.restapi.model.dto.tables.TableParam;
import onlyoriginal.restapi.service.DecoderService;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Base64;

@Service
@AllArgsConstructor
public class DecoderServiceImpl implements DecoderService {

    private final ObjectMapper mapper;

    @Override
    public TableParam decodeTableParamFromBase64(String tableParamEncoded) {

        String tableParamJson = new String(Base64.getUrlDecoder().decode(tableParamEncoded));
        TableParam tableParam;
        try {
            tableParam = mapper.readValue(tableParamJson, TableParam.class);
        } catch (IOException e) {
            throw new RuntimeException("TableParam json parse Error ");
        }

        return tableParam;
    }

    @Override
    public String encodeTableParamToBase64(TableParam tableParam) {

        String tableParamJson ;
        try {
            tableParamJson = mapper.writeValueAsString(tableParam);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("TableParam Base64 Encode Error");
        }

        return Base64.getUrlEncoder().withoutPadding().encodeToString(tableParamJson.getBytes());
    }

}
