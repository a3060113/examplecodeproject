package onlyoriginal.restapi.service.impl;


import lombok.NoArgsConstructor;
import onlyoriginal.restapi.model.dto.*;
import onlyoriginal.restapi.model.entity.*;
import onlyoriginal.restapi.service.ConverterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@NoArgsConstructor
public class ConverterServiceImpl implements ConverterService {


    @Autowired
    private Converter<Item, ItemDTO> itemToDtoConverter;

    @Autowired
    private Converter<ItemDTO, Item> itemFromDtoConverter;


    private Map<Class, Converter> entityToDtoConverterMap;
    private Map<Class, Converter> entityFromDtoConverterMap;

    @PostConstruct
    private void addConvertersToMaps() {
        entityToDtoConverterMap = new HashMap<>();
        entityFromDtoConverterMap = new HashMap<>();

        entityToDtoConverterMap.put(Item.class, itemToDtoConverter);
        entityFromDtoConverterMap.put(ItemDTO.class, itemFromDtoConverter);
    }

    public <D, E> D convertEntityToDto(@Nonnull E entity) {
        return entity == null ? null : (D) entityToDtoConverterMap.get(entity.getClass()).convert(entity);
    }

    public <D, E> E convertEntityFromDto(@Nonnull D dto) {
        return dto == null ? null : (E) entityFromDtoConverterMap.get(dto.getClass()).convert(dto);
    }

    public <D, E> List<D> convertEntitiesList(@Nonnull List<E> entityList) {
        return entityList.stream().map(this::convertEntityToDto)
                .map(o -> (D) o).collect(Collectors.toList());
    }

    public <D, E> List<D> convertEntitiesList(@Nonnull Iterable<E> entityList) {
        return StreamSupport.stream(entityList.spliterator(), false).map(this::convertEntityToDto)
                .map(o -> (D) o).collect(Collectors.toList());
    }

    public <E, D> List<E> convertDTOList(@Nonnull List<D> dtoList) {
        return dtoList.stream().map(this::convertEntityFromDto)
                .map(o -> (E) o).collect(Collectors.toList());
    }

    @Override
    public <D, E> Page<D> convertEntitiesList(Page<E> ePage) {
        List<D> entitiesDto = convertEntitiesList(ePage.getContent());
        return new PageImpl<>(entitiesDto, ePage.getPageable(), ePage.getTotalElements());
    }
}