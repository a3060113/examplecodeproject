package onlyoriginal.restapi.service.impl;

import lombok.AllArgsConstructor;
import onlyoriginal.restapi.dao.repo.ItemRepository;
import onlyoriginal.restapi.model.dto.ItemDTO;
import onlyoriginal.restapi.model.dto.tables.TableParam;
import onlyoriginal.restapi.service.ConverterService;
import onlyoriginal.restapi.service.ItemService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.util.UUID;

/**
 * Created by andrii on 7/15/18.
 */

@Service
@AllArgsConstructor
public class ItemServiceImpl implements ItemService {

    private final ItemRepository itemRepository;
    private final ConverterService converterService;

    @Override
    public ItemDTO getById (Long id) {
        return converterService.convertEntityToDto (itemRepository.findByIdItem (id));
    }

    @Override
    public ItemDTO update (ItemDTO dto) {
        return converterService.convertEntityToDto (itemRepository.save (converterService.convertEntityFromDto (dto)));
    }

    @Override
    public ItemDTO getItemByCode (UUID codeItem) {
        return converterService.convertEntityToDto (itemRepository.findByCodeItem (codeItem));
    }

    @Transactional
    @Override
    public ItemDTO create (@Nonnull ItemDTO dto) {
        return converterService.convertEntityToDto (itemRepository.save (converterService.convertEntityFromDto (dto)));
    }

    @Transactional
    @Override
    public void delete (Long idItem) {
        throw new UnsupportedOperationException ();
    }


    @Override
    public Page<ItemDTO> findAll (TableParam tableParam) {
        return converterService.convertEntitiesList (itemRepository.findAll (tableParam));
    }


}
