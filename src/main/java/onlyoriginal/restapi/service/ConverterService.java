package onlyoriginal.restapi.service;

import org.springframework.data.domain.Page;

import javax.annotation.Nonnull;
import java.util.List;

public interface ConverterService {

    <D, E> D convertEntityToDto(@Nonnull E entity);

    <D, E> E convertEntityFromDto(@Nonnull D dto);

    <D, E> List<D> convertEntitiesList(@Nonnull List<E> entityList);

    <D, E> List<D> convertEntitiesList(Iterable<E> entityList);

    <D, E> Page<D> convertEntitiesList(Page<E> entityList);

    <E, D> List<E> convertDTOList(List<D> dtoList);
}