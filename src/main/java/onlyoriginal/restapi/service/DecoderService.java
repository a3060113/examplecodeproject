package onlyoriginal.restapi.service;

import onlyoriginal.restapi.model.dto.tables.TableParam;

public interface DecoderService {

    TableParam decodeTableParamFromBase64 (String tableParam);

    String encodeTableParamToBase64(TableParam tableParam);
}
