package onlyoriginal.restapi.service;

public interface Crud<T> {

    T getById (Long id);

    T create (T dto);

    T update (T dto);

    void delete (Long id);

}
